const coin = {
        state: 0,
        flip: function() {
            // 1. One point: Randomly set your coin object's "state" property to be either 
            //    0 or 1: use "this.state" to access the "state" property on this object.
            this.state = Math.floor(Math.random() * 2);
            console.log(this.state);
        },
        toString: function() {
            if(this.state == 0)
                return "Heads";
            else
                return "Tails";
        },
        toHTML: function() {
            const image = document.createElement('img');
            // 3. One point: Set the properties of this image element to show either face-up
            //    or face-down, depending on whether this.state is 0 or 1.
            if(this.state == 0)
                image.src = "images/us-dollar-coin-front.jpg";
            else
                image.src = "images/us-dollar-coin-back.jpg";
            return image;
        }
    };
    display20Flips();
    function display20Flips() {
        const results = [];
        for(let i = 0; i < 20; i ++){
            coin.flip();
            let node = document.createElement("p");
            node.innerHTML = (coin.toString());
            document.getElementById("board").appendChild(node);
            results.push(coin.state);
        }
        return results;
        // 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.
    }
    display20Images();
    function display20Images() {
        const results = [];
        for(let i = 0; i < 20; i ++){
            coin.flip();
            let img = coin.toHTML();
            document.getElementById("board").appendChild(img);
            results.push(coin.state);
        }
        return results;
        // 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.
    }